-module(wss_webhook).
-author('ales@bublik.eu').
-export([wh_request/3]).

%%% web hooks
wh_request(_Storage, _Key, _Data) ->
	% wh urls
    lager:log(info, self(), wh_request),
	ok.
