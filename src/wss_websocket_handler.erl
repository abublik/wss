-module(wss_websocket_handler).

-export([init/2]).
-export([websocket_handle/3]).
-export([websocket_info/3]).
-export([terminate/3]).

-record(state, {
    bucket,
    resource,
    ets_storage
}).

init(Req, _Opts) ->
    lager:log(info, self(), init),
    Bucket = cowboy_req:binding(bucket, Req),
    Resource = cowboy_req:binding(resource, Req),
    EtsName = wss_storage:ets_name(Bucket, Resource),
    wss_storage:ets_table(EtsName),
    gproc:reg({p,l, EtsName}),
    % To prevent half connect state set timeout of 60 s
    {cowboy_websocket, Req, #state{bucket=Bucket,
                                   resource=Resource,
                                   ets_storage=EtsName}, 60000}.

websocket_handle({text, Msg}, Req, State) ->
    Json = jsx:encode(Msg),
    lager:log(info, self(), Json),
	Key = wss_storage:generate_new_key(State#state.bucket, State#state.resource),
    ok = wss_rest_resource_handler:insert(Key, Json, State#state.ets_storage),
    {ok, Req, State};
% TODO: Feature blocked. Maybe in the future release
websocket_handle({binary, Bin}, Req, State) ->
    lager:log(info, self(), Bin),
    %{reply, Frame, Req, State};
    {ok, Req, State};
websocket_handle(_Frame, Req, State) ->
    lager:log(info, self(), ws_handle),
    {ok, Req, State}.


websocket_info({timeout, _Ref, Msg}, Req, State) ->
    lager:log(info, self(), ws_timeout),
    {reply, {text, Msg}, Req, State};

websocket_info({log, Text}, Req, State) ->
    lager:log(info, self(), ws_info_log),
    {reply, {text, Text}, Req, State};

websocket_info({msg, {_Int, Msg}}, Req, State) ->
    lager:log(info, self(), ws_info_msg),
    {reply, {text, Msg}, Req, State};

websocket_info(Info, Req, State) ->
    lager:log(info, self(), ws_info),
	lager:log(info, self(), Info),
    {ok, Req, State}.


terminate(_Reason, _Req, _State) ->
    lager:log(info, self(), ws_terminate),
    ok.
