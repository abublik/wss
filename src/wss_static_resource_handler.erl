-module(wss_static_resource_handler).
-author('ales@bublik.eu').

-export([init/2]).

init(Req, Opts) ->
    {ok, cowboy_req:reply(200, [], <<"bla">>, Req), Opts}.
