-module(wss_loop_handler).
-bahaviour(cowboy_loop_handler).

-export([init/2]).
-export([info/3]).
-export([terminate/3]).

-record(state, {
    bucket,
    resource,
    key, value,
    ets_storage
}).

init(Req, _Opts) ->
    lager:log(info, self(), sse_init),
    Bucket = cowboy_req:binding(bucket, Req),
    Resource = cowboy_req:binding(resource, Req),
    EtsName = wss_storage:ets_name(Bucket, Resource),

    ID = cowboy_req:header(<<"last-event-id">>, Req, undefined),
	Headers = [{<<"content-type">>, <<"text/event-stream">>}],
	Req2 = cowboy_req:chunked_reply(200, Headers, Req),
    gproc:reg({p,l, EtsName}),
    {Key, Value} = kv_get(EtsName, ID),
    case Value of
        undefined ->
            ok;
        _ ->
            erlang:send(self(), {msg, {Key, Value}})
    end,
    {cowboy_loop, Req2, #state{ets_storage=EtsName,
							key=Key,
							value=Value}, 60000, hibernate}.

info({msg, {Key, Msg}}, Req, State) ->
    lager:log(info, self(), sse_msg),
    ok = cowboy_req:chunk([<<"id: ">>, integer_to_list(Key), <<"\ndata: ">>, Msg, <<"\n\n">>], Req),
    {ok, Req, State, hibernate};
info(_Message, Req, State) ->
    lager:log(info, self(), sse_info),
    {ok, Req, State, hibernate}.


terminate(_Reason, _Req, _State) ->
    lager:log(info, self(), sse_terminate),
    ok.


% Private %
%%%%%%%%%%%

kv_get(_EtsName, undefined) ->
    {undefined, undefined};

kv_get(EtsName, Key) when is_list(Key) ->
    kv_get(EtsName, list_to_integer(Key));

kv_get(EtsName, Key) when is_binary(Key) ->
    kv_get(EtsName, binary_to_integer(Key));

kv_get(EtsName, Key) when is_integer(Key) ->
    case ets:lookup(EtsName, Key) of
        [KeyVal] ->
            KeyVal;
        [] ->
            {Key, undefined}
    end.
