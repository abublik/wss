-module(wss_app).
-author('ales@bublik.eu').
-behaviour(application).

-export([start/2]).
-export([stop/1]).
-export([onresponse/4]).
-export([load_dispatch/0]).


onresponse(Code, Headers, Bin, Req) ->
    lager:log(info, self(), on_response),
    Headers2 = lists:keyreplace(<<"server">>, 1, Headers, {<<"server">>, <<"wss">>}),
    cowboy_req:reply(Code, Headers2, Bin, Req).

compile_dispatch() ->
    PrivDir = code:priv_dir(wss),
    {ok, [Routing]} = file:consult(filename:join([PrivDir, "routing"])),
    io:format("~p~n", [Routing]),
    Dispatch = cowboy_router:compile(Routing),
    Dispatch.

load_dispatch() ->
    Dispatch = compile_dispatch(),
    ok = cowboy:set_env(http, dispatch, Dispatch).

start(_Type, _Args) ->
    %% Ets for generating ids
    Options = [set, named_table, public, {write_concurrency, true}, {read_concurrency, true}],
    wss_counters = ets:new(wss_counters, Options),
    %% Name, NbAcceptors, TransOpts, ProtoOpts
    cowboy:start_http(http, 100,
        [{port, 8000}, {max_connections, 100}],
        [{env, [{dispatch, compile_dispatch()}]},
         {middlewares, [cowboy_router, wss_middleware, cowboy_handler]}
         %{onresponse, fun ?MODULE:onresponse/4}
        ]),
	%wss_webhook_sup:start_link(),
    wss_sup:start_link().

stop(_State) ->
    ok.
