-module(wss_handler).
-author('ales@bublik.eu').

-export([init/3]).
-export([handle/2]).
-export([terminate/3]).

init(_Type, Req, _Opts) ->
    {ok, Req, undefined_state}.

handle(Req, State) ->
    {Name, Req2} = cowboy_req:binding(name, Req),
    lager:log(info, self(), Name),
    {ok, Req3} = cowboy_req:reply(200, [
        {<<"content-type">>, <<"text/plain">>}],
        <<"Hello salamisti">>, Req2),
    {ok, Req3, State}.

terminate(_Reason, _Req, _State) ->
    ok.
