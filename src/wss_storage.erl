-module(wss_storage).
-author('ales@bublik.eu').

-export([ets_name/2]).
-export([generate_new_key/2]).
-export([ets_table/1]).

-define(COUNTER, key_counter).

ets_name(Bucket, Resource) ->
    % TODO: atom memory leak
    EtsName = binary_to_atom(list_to_binary([<<"wss">>, Bucket, Resource]), latin1),
    EtsName.

ets_table(EtsName) ->
    case ets:info(EtsName) of
        undefined ->
            Options = [set, named_table, public, {write_concurrency, true}, {read_concurrency, true}],
            EtsName = ets:new(EtsName, Options),
            ets:give_away(EtsName, whereis(wss_sup), []);
        _ ->
            void
    end.

generate_new_key(Bucket, Resource) ->
    Key = << Bucket/binary, Resource/binary >>,
    try ets:update_counter(wss_counters, Key, 1) of
        Int when is_integer(Int) ->
            Int
    catch
        error:_Error ->
            lager:log(info, self(), generate_new_key),
            ets:insert_new(wss_counters, {Key, 0}),
            generate_new_key(Bucket, Resource)
    end.

