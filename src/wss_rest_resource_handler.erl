-module(wss_rest_resource_handler).
-author('ales@bublik.eu').

-export([init/2]).
-export([content_types_provided/2]).
-export([content_types_accepted/2]).
-export([known_methods/2]).
-export([allowed_methods/2]).
-export([resource_exists/2]).
-export([to_json/2]).
-export([list/2]).
-export([delete_resource/2]).
-export([delete_completed/2]).
-export([to_html/2]).
-export([post_json/2]).
-export([ets_insert/3]).
-export([insert/3]).
-export([notify/3]).


-record(state, {
    method,
    bucket,
    resource,
    key, value,
    ets_storage
}).


init(Req, _Opts) ->
    lager:log(info, self(), init),
    Bucket = cowboy_req:binding(bucket, Req),
    Resource = cowboy_req:binding(resource, Req),
    Key = cowboy_req:binding(key, Req, undefined),
    Method = cowboy_req:method(Req),

    EtsName = wss_storage:ets_name(Bucket, Resource),
    wss_storage:ets_table(EtsName),
	lager:log(info, self(), supervisor:which_children(wss_sup)),
    {cowboy_rest, Req, #state{method=binary_to_existing_atom(Method, latin1),
                              bucket=Bucket,
                              resource=Resource,
                              key=Key,
                              ets_storage=EtsName}}.

content_types_provided(Req, State) ->
    lager:log(info, self(), content_types_provided),
	{[
		%{{<<"text">>, <<"html">>, []}, to_html},
		{{<<"application">>, <<"json">>, []}, to_json}
	], Req, State}.

content_types_accepted(Req, State) ->
    lager:log(info, self(), content_types_accepted),
    {[
		{{<<"application">>, <<"json">>, [{<<"charset">>, <<"utf-8">>}]}, post_json},
        {{<<"application">>, <<"json">>, []}, post_json}
    ], Req, State}.


known_methods(Req, State) ->
    lager:log(info, self(), known_methods),
    {[<<"GET">>, <<"HEAD">>, <<"POST">>, <<"PUT">>, <<"PATCH">>, <<"DELETE">>, <<"OPTIONS">>],
    Req, State}.

allowed_methods(Req, State=#state{key=undefined}) ->
    lager:log(info, self(), allowed_methods),
    {[<<"GET">>, <<"HEAD">>, <<"OPTIONS">>, <<"POST">>, <<"DELETE">>], Req, State};
allowed_methods(Req, State=#state{key=_Key}) ->
    lager:log(info, self(), allowed_methods),
    {[<<"GET">>, <<"HEAD">>, <<"OPTIONS">>, <<"POST">>, <<"PUT">>, <<"PATCH">>, <<"DELETE">>], Req, State}.


% create
resource_exists(Req, State=#state{method='POST', key=undefined}) ->
    lager:log(info, self(), resource_exists_post),
    % need to generate new key so
    {false, Req, State};
% listing
resource_exists(Req, State=#state{method='GET', key=undefined}) ->
    lager:log(info, self(), resource_exists),
    {true, Req, State};
resource_exists(Req, State=#state{method='DELETE', key=undefined}) ->
    lager:log(info, self(), resource_exists_list),
    {true, Req, State};
% any method on detail
resource_exists(Req, State=#state{key=Key}) ->
    case ets:lookup(State#state.ets_storage, Key) of
        [KeyVal] ->
            lager:log(info, self(), resource_exists_with_key),
            {true, Req, State#state{value=element(2, KeyVal)}};
        [] ->
            lager:log(info, self(), no_resource_exists),
            {false, Req, State}
    end.


delete_resource(Req, State=#state{key=undefined}) ->
    lager:log(info, self(), delete_resource),
    {ets:delete_all_objects(State#state.ets_storage), Req, State};
delete_resource(Req, State=#state{key=Key}) ->
    lager:log(info, self(), delete_resource),
    {ets:delete(State#state.ets_storage, Key), Req, State}.

delete_completed(Req, State) ->
    lager:log(info, self(), delete_completed),
    {true, Req, State}.


to_json(Req, State=#state{key=undefined}) ->
    % TODO: cache result to ets table and invalidate on save methods
    %       because converting all items to html is expensive
    list(Req, State);
to_json(Req, State=#state{value=Value}) when #state.key =/= undefined ->
    lager:log(info, self(), to_json),
    {Value, Req, State}.

list(Req, State=#state{method='GET'}) ->
    Data = ets:match(State#state.ets_storage, '$1'),
    Data2 = [Id || [{Id, _}] <- Data],
    % TODO: issue with
    {jsx:encode(Data2), Req, State}.

to_html(Req, State) ->
    lager:log(info, self(), to_html),
    to_json(Req, State).


post_json(Req, State) ->
    lager:log(info, self(), post_json),
    case cowboy_req:has_body(Req) of
        true ->
            % TODO: check the body size. See cowboy_req:body/2
            {ok, Data, Req2} = cowboy_req:body(Req),
            Url = cowboy_req:url(Req2),
            Key = wss_storage:generate_new_key(State#state.bucket, State#state.resource),
			case insert(Key, Data, State#state.ets_storage) of
				ok ->
					NewUrl = list_to_binary([Url, integer_to_binary(Key)]),
					{{true, NewUrl}, Req2, State};
				_ ->
					{false, Req2, State}
			end;
        false ->
            {false, Req, State}
    end.

% Private %%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

insert(Key, Data, Storage) ->
    case jsx:is_json(Data) of
        true ->
            true = ets_insert(Key, Data, Storage),
            notify(Storage, Key, Data),
            ok;
        false ->
            no_json
    end.

ets_insert(Key, Body, Storage) ->
    ets:insert_new(Storage, {Key, Body}).

notify(Storage, Key, Msg) ->
    gproc:send({p, l, Storage}, {msg, {Key, Msg}}),
	%wss_webhook:request(Storage, Key, Msg),
	%{ok, ConnPid} = gun:open("37.205.11.237", 8443, #{transport=>ssl}),
	%Ref = gun:get(ConnPid, "/"),
	%case gun:await(ConnPid, Ref) of
		%{response, _FinNofin, Status, _Headers} ->
			%lager:log(info, self(), Status),
			%ok
	%end,
	%gun:shutdown(ConnPid).
	ok.
