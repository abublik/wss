-module(wss_webhook_sup).
-export([start_link/0, init/1]).
-behaviour(supervisor).

start_link() ->
	supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init(_Args) ->
	{ok, {restart_strategy(), workers()}}.

restart_strategy() ->
	{one_for_one, 10, 10}.

workers() ->
	%[worker_spec()].
    [].

%worker_spec() ->
	% How workers should be managed.
	%{chfeeds,                       %% A name for our worker spec.
	%{feeds_gen, start_link, []},    %% The module, function and params of our workers.
									% Worker's execution entry point.
	%transient,                      %% How the worker should be restarted.
									% transient means restart only if it abnormally
									% terminated.
	%3000,                           %% Milliseconds to wait for worker's shutdown.
									% After that time the supervisor will abruptly
									% terminate it.
	%worker,                         %% Defines a subordinate process type, a worker
									% in this case, but it could has been another
									% supervisor.
	%[feeds_sup]}.                   %% One element list with the worker's module.

