-module(wss_dashboard).
-author('ales@bublik.eu').

-export([init/3]).
-export([info/3]).
-export([terminate/3]).

-record(state, {
    }).

init({tcp, http}, Req, _Opts) ->
    lager:log(info, self(), dash_init),

    {_ID, Req2} = cowboy_req:header(<<"last-event-id">>, Req, undefined),
	Headers = [{<<"content-type">>, <<"text/event-stream">>}],
    {ok, Req3} = cowboy_req:chunked_reply(200, Headers, Req2),
    {loop, Req3, #state{}}.

info({msg, Msg}, Req, State) ->
    ok = cowboy_req:chunk([<<"id: ">>, integer_to_list(5), <<"\ndata: ">>, Msg, <<"\n\n">>], Req),
    {loop, Req, State, hibernate};
info(_Message, Req, State) ->
    lager:log(info, self(), dash_info),
    {loop, Req, State, hibernate}.


terminate(_Reason, _Req, _State) ->
    lager:log(info, self(), dash_terminate),
    ok.


