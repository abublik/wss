-module(wss_middleware).
-author('ales@bublik.eu').

-behaviour(cowboy_middleware).

-export([execute/2]).

execute(Req, Env) ->
    lager:log(info, self(), '_________'),
    {ok, Req, Env}.
