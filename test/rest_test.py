# -*- coding: utf-8 -*-

# pip install webtest
# pip install WSGIProxy2
#
#

from webtest import TestApp

app = TestApp('http://127.0.0.1:8000#requests')
#app = TestApp('http://95.85.5.59:8080#requests')
path_resource = '/api/tbucket/tresource/'
path_detail = path_resource + "{key}/"


def create_item(data_dict):
    resp = app.post_json(path_resource, data_dict)
    assert 'Location' in resp.headers
    return resp

def get_already_stored_item(url):
    json_mimetype = 'application/json'
    resp = app.get(url, headers={'Accept': json_mimetype})
    assert resp.content_type == json_mimetype
    return resp

def get_nonexisting_item(url):
    resp = app.get(url, headers={'Accept': 'application/json'}, status=404)
    return resp


def test_create_and_get_item():
    data_dict = {"name": "joe", "surname": 'armstron', "lastname": 12}
    resp = create_item(data_dict)
    resp2 = get_already_stored_item(resp.headers['Location'])
    assert resp2.json == data_dict
    assert resp2.content_type == 'application/json'

def test_delete_item():
    data_dict = {"name": "foo", "surname": [1,2,3]}
    resp = create_item(data_dict)

    resp_delete = app.delete(resp.headers['Location'])
    assert resp_delete.status_int == 204

    resp_non = get_nonexisting_item(resp.headers['Location'])
    resp_non.status_int = 404


#def test_unicode_create_and_get_item():
#    data_dict = {"Příliš": "Žluťoučký kůň úpěl ďábelskou ódu"}
#    resp = create_item(data_dict)
#    resp2 = get_already_stored_item(resp.headers['Location'])
#    import ipdb; ipdb.set_trace()
#    assert resp2.content_type == 'application/json'
#    assert resp2.json == data_dict
#

if __name__ == '__main__':
    local_objects = locals()

    for key in local_objects.keys():
        if key.startswith('test_'):
            local_fce = local_objects[key]
            print(local_fce.func_name)
            local_fce()
