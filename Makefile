PROJECT = wss

#DEPS = bitcask lager jiffy msgpack jsx erlydtl gproc cowboy
DEPS = lager jsx gproc jobs gun cowboy

dep_bitcask = git https://github.com/basho/bitcask.git master
dep_lager = git https://github.com/basho/lager.git master
dep_gproc = git https://github.com/uwiger/gproc.git master
dep_msgpack = git https://github.com/msgpack/msgpack-erlang.git master
dep_jiffy = git https://github.com/davisp/jiffy.git
dep_jsx = git https://github.com/talentdeficit/jsx.git
dep_erlydtl = git https://github.com/erlydtl/erlydtl.git
dep_cowboy = git https://github.com/ninenines/cowboy.git master
dep_gun = git https://github.com/ninenines/gun.git master
dep_jobs = git https://github.com/uwiger/jobs.git master
# dep_clique = git https://github.com/basho/clique.git develop

SHELL_DEPS = sync
dep_sync = git git://github.com/inaka/sync.git 0.1.3

include erlang.mk

ERLC_OPTS += +'{parse_transform, lager_transform}'
TEST_ERLC_OPTS += +'{parse_transform, lager_transform}'

CONFIG = rel/sys.config

SHELL_OPTS = -name ${PROJECT}@`hostname` -s ${PROJECT} -config ${CONFIG} -s sync
