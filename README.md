Web Server Services

There is problem with unicode when list resources. jsx, jiffy ...

https://github.com/basho/cuttlefish


Generic Key Value Storage
- implement generic key value storage for various backends i.e. RAM, bitcask, redis, postgresql, mysql
- key is always integer
- value can be text or JSON
- implement MsgPack


WebSockets
- Performance tests with the help of tsung
- enable storing messages in the way REST api does + notification for other protocols
- investigate possibility for SSL


SSE
- Bind Last-Event-ID header with storage and get data saved with that key
- Performance tests
- investigate possibility for SSL
- Support for reconnecting with the help of ID

LOCKING SERVICE
- optimistic
- pesimistic



Optimizations
+bin_opt_info
